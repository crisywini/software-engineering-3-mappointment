package co.uniquindio.software3.mappointment.presentation.main_menu.presenter

import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.usecase.medicalappointment.GetMedicalAppointmentsByUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MedicalAppointmentsPresenter(
    private val view: View?,
    private val getMedicalAppointmentsByUser: GetMedicalAppointmentsByUser
) :
    CoroutineScope {

    interface View {
        fun loadMedicalAppointments(userId: String)
        fun loadDataSet(dataset: List<MedicalAppointment>)
        fun startReasonActivity(view: android.view.View)
    }

    /**
     * Need the next right things
     * private val medicalAppointmentsAndDate: List<MedicalAppointmentAndDate>,
     * private val medicalAppointmentsAndDoctorAndMedicalAppointment: List<DoctorAndMedicalAppointment>,
     * private val medicalAppointmentAndStatus: List<MedicalAppointmentAndStatus>
     */

    private val job = Job()//If one coroutine fail, all the coroutines fails

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job


    fun getMedicalAppointmentByUserId(userId: String) {
        var medicalAppointments = listOf<MedicalAppointment>()
        launch {
            medicalAppointments = getMedicalAppointmentsByUser.invoke(userId)
            view?.loadDataSet(medicalAppointments)
        }
    }


}