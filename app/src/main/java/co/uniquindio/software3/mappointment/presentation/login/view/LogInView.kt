package co.uniquindio.software3.mappointment.presentation.login.view

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import co.uniquindio.software3.data.user.UserRepositoryImpl
import co.uniquindio.software3.mappointment.R
import co.uniquindio.software3.mappointment.framework.database.user.PersistenceUserSource
import co.uniquindio.software3.mappointment.MedicalAppointmentApp
import co.uniquindio.software3.mappointment.presentation.login.presenter.LogInPresenter
import co.uniquindio.software3.mappointment.presentation.main_menu.view.MedicalAppointmentsView
import co.uniquindio.software3.usecase.user.LogInUser
import co.uniquindio.software3.usecase.user.SaveUser
import kotlinx.android.synthetic.main.activity_auth.*
import android.Manifest
import co.uniquindio.software3.data.date.DateRepositoryImpl
import co.uniquindio.software3.data.doctor.DoctorRepositoryImpl
import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepositoryImpl
import co.uniquindio.software3.data.reason.ReasonRepositoryImpl
import co.uniquindio.software3.data.status.StatusRepositoryImpl
import co.uniquindio.software3.domain.model.User
import co.uniquindio.software3.mappointment.framework.database.date.PersistenceDateSource
import co.uniquindio.software3.mappointment.framework.database.doctor.PersistenceDoctorSource
import co.uniquindio.software3.mappointment.framework.database.medical_appointment.PersistenceMedicalAppointmentSource
import co.uniquindio.software3.mappointment.framework.database.reason.PersistenceReasonSource
import co.uniquindio.software3.mappointment.framework.database.status.PersistenceStatusSource
import co.uniquindio.software3.usecase.date.SaveDate
import co.uniquindio.software3.usecase.doctor.SaveDoctor
import co.uniquindio.software3.usecase.medicalappointment.SaveMedicalAppointment
import co.uniquindio.software3.usecase.reason.SaveReason
import co.uniquindio.software3.usecase.status.SaveStatus

class LogInView : AppCompatActivity(),
    LogInPresenter.View {

    private lateinit var presenter: LogInPresenter
    private lateinit var app: MedicalAppointmentApp
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setup()
        app = applicationContext as MedicalAppointmentApp
        val persistenceUser = PersistenceUserSource(app)
        val userRepository = UserRepositoryImpl(persistenceUser)
        val persistenceDate = PersistenceDateSource(app)
        val dateRepository = DateRepositoryImpl(persistenceDate)
        val persistenceStatus = PersistenceStatusSource(app)
        val statusRepository = StatusRepositoryImpl(persistenceStatus)
        val persistenceReason = PersistenceReasonSource(app)
        val reasonRepository = ReasonRepositoryImpl(persistenceReason)
        val persistenceDoctor = PersistenceDoctorSource(app)
        val doctorRepository = DoctorRepositoryImpl(persistenceDoctor)
        val persistenceMedicalAppointment = PersistenceMedicalAppointmentSource(app)
        val medicalAppointmentRepository =
            MedicalAppointmentRepositoryImpl(persistenceMedicalAppointment)
        presenter = LogInPresenter(
            this,
            LogInUser(userRepository),
            SaveUser(userRepository),
            SaveDate(dateRepository),
            SaveStatus(statusRepository),
            SaveReason(reasonRepository),
            SaveDoctor(doctorRepository),
            SaveMedicalAppointment(medicalAppointmentRepository)
        )
        askPermissions()
    }

    private fun askPermissions() {
        val permissionAll = 1
        val permission = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        )
        if (!hasPermissions(this, *permission)) {
            ActivityCompat.requestPermissions(this, permission, permissionAll)
        }
    }

    private fun hasPermissions(context: Context?, vararg permissions: String?): Boolean {
        if (context != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission!!
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }

    private fun setup() {
        logInButton.setOnClickListener {
            signIn()
        }
    }

    override fun showMedicalAppointmentsView(user: User) {
        val medicalAppointmentIntent =
            Intent(this, MedicalAppointmentsView::class.java)
        medicalAppointmentIntent.putExtra("userId", user.userId)

        startActivity(medicalAppointmentIntent)
    }

    override fun showError(errorMessage: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(errorMessage)
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun cleanFields() {
        emailEditText.setText("")
        passwordEditText.setText("")
    }

    private fun signIn() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        presenter.signInWithEmailAndPassword(email, password)
    }

    /*private fun showRegister(email: String) {
        val registerIntent: Intent = Intent(this, RegisterView::class.java).apply {
            putExtra("email", email)
        }
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(registerIntent)
        //finish()
    }*/

    override fun onDestroy() {
        presenter.detachJob()
        super.onDestroy()
    }

    override fun onDetachedFromWindow() {
        presenter.detachJob()
        super.onDetachedFromWindow()
    }

}