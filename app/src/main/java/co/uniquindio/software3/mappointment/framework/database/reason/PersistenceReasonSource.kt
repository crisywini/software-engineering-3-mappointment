package co.uniquindio.software3.mappointment.framework.database.reason

import co.uniquindio.software3.data.reason.PersistenceReasonDataSource
import co.uniquindio.software3.domain.model.Reason
import co.uniquindio.software3.mappointment.MedicalAppointmentApp

class PersistenceReasonSource(private val context: MedicalAppointmentApp) :
    PersistenceReasonDataSource {
    override suspend fun save(reason: Reason) {
        context.room.reasonDao().saveReason(reason)
    }

    override suspend fun getAll(): List<Reason> = context.room.reasonDao().getReasons()

}