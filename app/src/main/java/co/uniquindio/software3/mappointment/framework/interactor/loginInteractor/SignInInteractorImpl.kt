package co.uniquindio.software3.mappointment.framework.interactor.loginInteractor

import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

class SignInInteractorImpl : SignInInteractor {
    override suspend fun signInWithEmailAndPassword(
        email: String,
        password: String
    ) {
        var unit: Unit = suspendCancellableCoroutine { continuation ->
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        continuation.resume(Unit)
                    } 
                }
        }
    }
}