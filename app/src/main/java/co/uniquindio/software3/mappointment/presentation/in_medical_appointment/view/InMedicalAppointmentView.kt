package co.uniquindio.software3.mappointment.presentation.in_medical_appointment.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import co.uniquindio.software3.mappointment.R
import co.uniquindio.software3.mappointment.presentation.in_medical_appointment.view.adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_in_medical_appointment.*

class InMedicalAppointmentView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_medical_appointment)

        //The last thing that need to be done
        configureViewPager()
        onChangeTab(0)
    }

    private fun configureViewPager() {
        val adapter: ViewPagerAdapter = ViewPagerAdapter(supportFragmentManager, 0)
        fragmentContainerViewPager.adapter = adapter
        fragmentContainerViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                onChangeTab(position)
            }
        })
    }

    private fun onChangeTab(position: Int) {

    }
}