package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import co.uniquindio.software3.domain.model.Status
import co.uniquindio.software3.domain.model.StatusAndMedicalAppointment

@Dao
interface StatusDAO {
    @Insert
    suspend fun saveStatus(status: Status)


    @Query("SELECT * FROM Status WHERE status_id = :statusId LIMIT 1")
    suspend fun getStatusById(statusId: Int): Status?

    @Transaction
    @Query("SELECT * FROM Status")
    suspend fun getMedicalAppointmentsAndStatus(): List<StatusAndMedicalAppointment>

}