package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import co.uniquindio.software3.domain.model.Symptom
import co.uniquindio.software3.domain.model.SymptomWithMedicalAppointment

@Dao
interface SymptomDAO {

    @Insert
    suspend fun saveSymptom(symptom: Symptom)

    @Transaction
    @Query("SELECT * FROM Symptom")
    suspend fun getSymptomsWithMedicalAppointment(): List<SymptomWithMedicalAppointment>
}