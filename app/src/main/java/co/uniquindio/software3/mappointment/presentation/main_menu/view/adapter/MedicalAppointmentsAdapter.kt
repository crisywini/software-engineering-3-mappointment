package co.uniquindio.software3.mappointment.presentation.main_menu.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.uniquindio.software3.domain.model.*
import co.uniquindio.software3.mappointment.R
import co.uniquindio.software3.usecase.date.GetDate
import co.uniquindio.software3.usecase.doctor.GetDoctor
import co.uniquindio.software3.usecase.status.GetStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MedicalAppointmentsAdapter(
    private val listener: View.OnClickListener,
    private val getStatus: GetStatus,
    private val getDoctor: GetDoctor,
    private val getDate: GetDate
) :
    RecyclerView.Adapter<MedicalAppointmentsAdapter.ViewHolder>(), CoroutineScope {

    private val job = Job()//If one coroutine fail, all the coroutines fails

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job


    private var medicalAppointments = listOf<MedicalAppointment>()

    fun setDatasets(medicalAppointment: List<MedicalAppointment>) {
        this.medicalAppointments = medicalAppointment
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateHourMedicalAppointmentTextView: TextView =
            itemView.findViewById(R.id.dateHourMedicalAppointmentTextView)
        val inEditTextView: TextView = itemView.findViewById(R.id.inEditTextView)
        val doctorTextView: TextView = itemView.findViewById(R.id.doctorTextView)
        val layout: LinearLayout = itemView.findViewById(R.id.medicalAppointmentRootLayout)
        val medicalAppointmentIdTextView: TextView =
            itemView.findViewById(R.id.medicalAppointmentIdTextView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.medical_appointment_item, parent, false)
        //view.setOnClickListener(listener)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        launch {
            val medicalAppointment = medicalAppointments[position]
            val status = getStatus.invoke(medicalAppointment.currentStatusId)
            val doctor = getDoctor.invoke(medicalAppointment.doctorOwnerId)
            val date = getDate.invoke(medicalAppointment.currentDateId)
            val medicalAppointmentId = medicalAppointment.medicalAppointmentId
            holder.medicalAppointmentIdTextView.text = medicalAppointmentId.toString()
            holder.doctorTextView.text = "Dr. " + doctor!!.name + " " + doctor!!.lastName
            holder.dateHourMedicalAppointmentTextView.text = date.toString()
            holder.layout.setOnClickListener(listener)
            if (status!!.statusId == 1) {
                holder.inEditTextView.text = "Entrar"
            } else if (status.statusId == 2) {
                holder.inEditTextView.text = "Modificar"
            }
        }
    }

    override fun getItemCount(): Int {
        return medicalAppointments.size
    }
}