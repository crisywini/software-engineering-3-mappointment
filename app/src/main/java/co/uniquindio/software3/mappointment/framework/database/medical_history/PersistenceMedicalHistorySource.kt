package co.uniquindio.software3.mappointment.framework.database.medical_history

import co.uniquindio.software3.data.medical_history.PersistenceMedicalHistoryDataSource
import co.uniquindio.software3.domain.model.MedicalHistory
import co.uniquindio.software3.mappointment.MedicalAppointmentApp

class PersistenceMedicalHistorySource(private val context: MedicalAppointmentApp) :
    PersistenceMedicalHistoryDataSource {
    override suspend fun getMedicalHistories(): List<MedicalHistory> =
        context.room.medicalHistoryDao().getAllMedicalHistories()

    override suspend fun saveMedicalHistory(medicalHistory: MedicalHistory) {
        context.room.medicalHistoryDao().saveMedicalHistory(medicalHistory)
    }
}