package co.uniquindio.software3.mappointment.framework.database.user

import android.util.Log
import co.uniquindio.software3.data.user.PersistenceUserDataSource
import co.uniquindio.software3.domain.model.User
import co.uniquindio.software3.mappointment.MedicalAppointmentApp


class PersistenceUserSource(private val context: MedicalAppointmentApp): PersistenceUserDataSource {


    override suspend fun logInUser(email: String, password: String): User? {
        val user =context.room.userDao().logIn(email, password)
        Log.e("logInUser: ", "USER COULD BE $user")
        return user
    }

    override suspend fun save(user: User) {
        context.room.userDao().insert(user)
    }

    override suspend fun getById(id: String): User? {
        return context.room.userDao().getById(id)
    }

    override suspend fun update(user: User) {
        context.room.userDao().update(user)
    }

    override suspend fun remove(user: User): User? {
        context.room.userDao().delete(user)
        return null
    }

    override suspend fun getAll(): List<User> {
        return context.room.userDao().getAll()
    }

    override suspend fun exists(id: String): Boolean {
        return true
    }
}