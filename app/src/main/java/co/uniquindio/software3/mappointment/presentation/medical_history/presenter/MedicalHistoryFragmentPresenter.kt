package co.uniquindio.software3.mappointment.presentation.medical_history.presenter

import co.uniquindio.software3.domain.model.MedicalHistory
import co.uniquindio.software3.usecase.medical_history.GetAllMedicalHistories
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MedicalHistoryFragmentPresenter(
    private val view: View?,
    private val getAllMedicalHistories: GetAllMedicalHistories
) : CoroutineScope {
    interface View {
        fun initCheckboxData(medicalHistories: List<MedicalHistory>)
    }

    private val job = Job()//If one coroutine fail, all the coroutines fails

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    fun setMedicalHistories() {
        launch {
            val medicalHistories = getAllMedicalHistories.invoke()
            view?.initCheckboxData(medicalHistories)
        }
    }
}