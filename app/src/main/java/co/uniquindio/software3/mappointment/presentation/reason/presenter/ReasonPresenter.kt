package co.uniquindio.software3.mappointment.presentation.reason.presenter

import android.util.Log
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.Reason
import co.uniquindio.software3.usecase.medicalappointment.GetMedicalAppointment
import co.uniquindio.software3.usecase.medicalappointment.UpdateMedicalAppointment
import co.uniquindio.software3.usecase.reason.GetAllReasons
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ReasonPresenter(
    private val view: View?,
    private val getAllReasons: GetAllReasons,
    private val getMedicalAppointment: GetMedicalAppointment,
    private val updateMedicalAppointment: UpdateMedicalAppointment
) : CoroutineScope {

    private val job = Job()//If one coroutine fail, all the coroutines fails

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    interface View {
        fun loadReasons(reasons: List<Reason>)
        fun initialize(medicalAppointment: MedicalAppointment?)
        fun startMedicalHistoryActivity(view: android.view.View)
    }

    fun getReasons() {
        var reasons: List<Reason>
        launch {
            reasons = getAllReasons.invoke()
            view?.loadReasons(reasons)
        }
    }

    fun setMedicalAppointment(medicalAppointmentId: Int) {
        var medicalAppointment: MedicalAppointment? = null
        launch {
            medicalAppointment = getMedicalAppointment.invoke(medicalAppointmentId)
            view?.initialize(medicalAppointment)
        }
    }

    fun updateMedicalAppointment(medicalAppointment: MedicalAppointment) {
        launch {
            updateMedicalAppointment.invoke(medicalAppointment)
        }
    }
}
