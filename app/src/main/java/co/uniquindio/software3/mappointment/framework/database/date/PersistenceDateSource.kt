package co.uniquindio.software3.mappointment.framework.database.date

import co.uniquindio.software3.data.date.PersistenceDateDataSource
import co.uniquindio.software3.domain.model.Date
import co.uniquindio.software3.mappointment.MedicalAppointmentApp

class PersistenceDateSource(private val context: MedicalAppointmentApp) :
    PersistenceDateDataSource {
    override suspend fun save(date: Date) {
        context.room.dateDao().saveDate(date)
    }

    override suspend fun get(id: Int): Date? {
        return context.room.dateDao().getDate(id)
    }
}