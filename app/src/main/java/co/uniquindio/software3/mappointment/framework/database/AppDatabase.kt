package co.uniquindio.software3.mappointment.framework.database

import androidx.room.Database
import androidx.room.RoomDatabase
import co.uniquindio.software3.domain.model.*
import co.uniquindio.software3.mappointment.framework.database.dao.*

@Database(
    entities = [User::class, Date::class, Measure::class,
        MedicalAppointment::class,
        MedicalHistory::class, Reason::class, Status::class, Symptom::class, Doctor::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDAO
    abstract fun doctorDao(): DoctorDAO
    abstract fun medicalAppointmentDao(): MedicalAppointmentDAO
    abstract fun dateDao(): DateDAO
    abstract fun reasonDao(): ReasonDAO
    abstract fun statusDao(): StatusDAO
    abstract fun medicalHistoryDao(): MedicalHistoryDAO
    abstract fun symptomDao(): SymptomDAO
}