package co.uniquindio.software3.mappointment.presentation.reason.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.Reason
import co.uniquindio.software3.mappointment.R

class ReasonsAdapter(
    private val listener: View.OnClickListener
) :
    RecyclerView.Adapter<ReasonsAdapter.ViewHolder>() {
    private var reasons: List<Reason> = listOf()

    fun setDataset(reasons: List<Reason>) {
        this.reasons = reasons
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val reasonImageView: ImageView = itemView.findViewById(R.id.reasonImageView)
        val reasonDescriptionTextView: TextView =
            itemView.findViewById(R.id.reasonDescriptionTextView)
        val layout: LinearLayout = itemView.findViewById(R.id.reasonRootLayout)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReasonsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.reason_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReasonsAdapter.ViewHolder, position: Int) {
        val reason = reasons[position]
        val description = reason.reasonDescription
        holder.reasonDescriptionTextView.text = reason.reasonDescription
        holder.reasonDescriptionTextView.tag = reason.reasonId
        holder.layout.setOnClickListener(listener)
        when (description) {
            "Chequeo" -> {
                holder.reasonImageView.setBackgroundResource(R.drawable.ic_chequeo)
            }
            "Revisi�n pre-especialista" -> {
                holder.reasonImageView.setBackgroundResource(R.drawable.ic_revision_especialista)

            }
            "Revisi�n por sintomas" -> {
                holder.reasonImageView.setBackgroundResource(R.drawable.ic_revision_sintomas)

            }
            "Revisi�n por resultados" -> {
                holder.reasonImageView.setBackgroundResource(R.drawable.ic_revision_resultados)
            }
        }

    }

    override fun getItemCount(): Int = reasons.size


}