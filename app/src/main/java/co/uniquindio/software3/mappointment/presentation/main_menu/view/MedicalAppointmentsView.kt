package co.uniquindio.software3.mappointment.presentation.main_menu.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.uniquindio.software3.data.date.DateRepository
import co.uniquindio.software3.data.date.DateRepositoryImpl
import co.uniquindio.software3.data.doctor.DoctorRepositoryImpl
import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepositoryImpl
import co.uniquindio.software3.data.status.StatusRepository
import co.uniquindio.software3.data.status.StatusRepositoryImpl
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.UserWithMedicalAppointments
import co.uniquindio.software3.mappointment.R
import co.uniquindio.software3.mappointment.framework.database.medical_appointment.PersistenceMedicalAppointmentSource
import co.uniquindio.software3.mappointment.MedicalAppointmentApp
import co.uniquindio.software3.mappointment.framework.database.date.PersistenceDateSource
import co.uniquindio.software3.mappointment.framework.database.doctor.PersistenceDoctorSource
import co.uniquindio.software3.mappointment.framework.database.status.PersistenceStatusSource
import co.uniquindio.software3.mappointment.presentation.main_menu.presenter.MedicalAppointmentsPresenter
import co.uniquindio.software3.mappointment.presentation.main_menu.view.adapter.MedicalAppointmentsAdapter
import co.uniquindio.software3.mappointment.presentation.reason.view.ReasonView
import co.uniquindio.software3.usecase.date.GetDate
import co.uniquindio.software3.usecase.doctor.GetDoctor
import co.uniquindio.software3.usecase.medicalappointment.GetMedicalAppointmentsByUser
import co.uniquindio.software3.usecase.status.GetStatus
import kotlinx.android.synthetic.main.activity_medical_agendas.*

class MedicalAppointmentsView : AppCompatActivity(), MedicalAppointmentsPresenter.View {

    private lateinit var presenter: MedicalAppointmentsPresenter
    private lateinit var app: MedicalAppointmentApp
    private lateinit var userId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medical_agendas)
        getExtras()
        setTitle(R.string.medical_appointment_view_title)
        app = applicationContext as MedicalAppointmentApp
        val persistenceMedicalAppointmentSource = PersistenceMedicalAppointmentSource(app)
        val medicalAppointmentRepository =
            MedicalAppointmentRepositoryImpl(persistenceMedicalAppointmentSource)
        presenter =
            MedicalAppointmentsPresenter(
                this,
                GetMedicalAppointmentsByUser(medicalAppointmentRepository)
            )

        setUpRecyclerView()
        loadMedicalAppointments(userId)
    }

    private fun getExtras() {
        val bundle = intent.extras
        if (bundle != null) {
            userId = bundle.get("userId") as String
        }
    }

    private fun setUpRecyclerView() {
        val doctorSource = PersistenceDoctorSource(app)
        val doctorRepository = DoctorRepositoryImpl(doctorSource)
        val dateSource = PersistenceDateSource(app)
        val dateRepository = DateRepositoryImpl(dateSource)
        val statusSource = PersistenceStatusSource(app)
        val statusRepository = StatusRepositoryImpl(statusSource)
        val adapter = MedicalAppointmentsAdapter(
            View.OnClickListener {
                startReasonActivity(it)
            },
            GetStatus(statusRepository),
            GetDoctor(doctorRepository),
            GetDate(dateRepository)
        )
        agendasRecyclerView.adapter = adapter
    }

    override fun loadMedicalAppointments(userId: String) {
        presenter.getMedicalAppointmentByUserId(userId)
    }

    override fun loadDataSet(dataset: List<MedicalAppointment>) {
        val adapter = agendasRecyclerView.adapter as MedicalAppointmentsAdapter
        adapter.setDatasets(dataset)
    }

    override fun startReasonActivity(view: View) {
        val medicalAppointmentIdTextView =
            view.findViewById<TextView>(R.id.medicalAppointmentIdTextView)
        val medicalAppointmentId = medicalAppointmentIdTextView.text.toString()
        val reasonIntent = Intent(this, ReasonView::class.java)
        reasonIntent.putExtra("medicalAppointmentId", medicalAppointmentId)
        startActivity(reasonIntent)
    }
}