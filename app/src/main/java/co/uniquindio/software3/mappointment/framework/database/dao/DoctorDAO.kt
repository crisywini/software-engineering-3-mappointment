package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import co.uniquindio.software3.domain.model.Doctor
import co.uniquindio.software3.domain.model.DoctorAndMedicalAppointment

@Dao
interface DoctorDAO {

    @Transaction
    @Query("SELECT * FROM Doctor")
    suspend fun getMedicalAppointmentsAndDoctor(): List<DoctorAndMedicalAppointment>

    @Insert
    suspend fun saveDoctor(doctor: Doctor)

    @Query("SELECT * FROM Doctor WHERE doctor_id = :doctorId LIMIT 1")
    suspend fun getDoctorById(doctorId: String): Doctor?
}