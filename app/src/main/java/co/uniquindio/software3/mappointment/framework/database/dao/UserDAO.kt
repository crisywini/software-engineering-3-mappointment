package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.*
import co.uniquindio.software3.data.user.UserRepository
import co.uniquindio.software3.domain.model.User
import co.uniquindio.software3.domain.model.UserWithMedicalAppointments

@Dao
interface UserDAO{
    @Query("SELECT * FROM User")
    suspend fun getAll() : List<User>
    @Insert
    suspend fun insert(user: User)
    @Delete
    suspend fun delete(user: User)

    @Query("SELECT * FROM User WHERE user_id = :id")
    suspend fun getById(id: String):User?
    @Update
    suspend fun update(user: User)

    @Query("SELECT * FROM User WHERE email = :email AND password = :password LIMIT 1")
    suspend fun logIn(email: String, password: String): User?

    @Transaction
    @Query("SELECT * FROM User")
    suspend fun getUserWithMedicalAppointments(): List<UserWithMedicalAppointments>

    @Transaction
    @Query("SELECT * FROM User WHERE user_id = :userId ")
    suspend fun getUserWithMedicalAppointmentsById(userId:String): List<UserWithMedicalAppointments>

}