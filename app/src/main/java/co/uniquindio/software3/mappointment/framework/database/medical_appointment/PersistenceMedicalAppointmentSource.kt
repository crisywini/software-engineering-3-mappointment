package co.uniquindio.software3.mappointment.framework.database.medical_appointment

import co.uniquindio.software3.data.medical_appointment.PersistenceMedicalAppointmentDataSource
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.UserWithMedicalAppointments
import co.uniquindio.software3.mappointment.MedicalAppointmentApp

class PersistenceMedicalAppointmentSource(private val context: MedicalAppointmentApp) :
    PersistenceMedicalAppointmentDataSource {

    override suspend fun getMedicalAppointmentsByUser(userId: String): List<MedicalAppointment> {
        return context.room.medicalAppointmentDao().getMedicalAppointmentByUser(userId)
    }

    override suspend fun save(medicalAppointment: MedicalAppointment) {
        context.room.medicalAppointmentDao().saveMedicalAppointment(medicalAppointment)
    }

    override suspend fun getMedicalAppointmentById(medicalAppointmentId: Int): MedicalAppointment? {
        return context.room.medicalAppointmentDao().getMedicalAppointmentById(medicalAppointmentId)
    }

    override suspend fun updateMedicalAppointment(medicalAppointment: MedicalAppointment) {
        context.room.medicalAppointmentDao().updateMedicalAppointment(medicalAppointment)
    }
}