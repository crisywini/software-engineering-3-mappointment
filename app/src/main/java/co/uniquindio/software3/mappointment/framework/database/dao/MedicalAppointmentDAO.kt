package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.*
import co.uniquindio.software3.domain.model.*

@Dao
interface MedicalAppointmentDAO {
    @Insert
    suspend fun saveMedicalAppointment(medicalAppointment: MedicalAppointment)

    @Query("SELECT * FROM MedicalAppointment WHERE medical_appointment_id = :id")
    suspend fun getMedicalAppointmentById(id: Int): MedicalAppointment?

    @Transaction
    @Query("SELECT * FROM MedicalAppointment")
    suspend fun getMedicalAppointmentsWithSymptoms(): List<MedicalAppointmentWithSymptoms>

    @Transaction
    @Query("SELECT * FROM MedicalAppointment")
    suspend fun getMedicalAppointmentWithMedicalHistory(): List<MedicalHistoryWithMedicalAppointment>

    @Query("SELECT * FROM Doctor d INNER JOIN MedicalAppointment ma ON d.doctor_id = ma.doctorOwnerId WHERE ma.userOwnerId = :userId")
    suspend fun getMedicalAppointmentAndDoctorsByUserId(userId: String): List<DoctorAndMedicalAppointment>

    @Query("SELECT * FROM MedicalAppointment WHERE userOwnerId = :userId")
    suspend fun getMedicalAppointmentByUser(userId: String): List<MedicalAppointment>

    @Update
    suspend fun updateMedicalAppointment(medicalAppointment: MedicalAppointment)

    @Transaction
    @Query("SELECT * FROM MedicalAppointment")
    suspend fun getMedicalAppointmentsWithMedicalHistory(): List<MedicalAppointmentWithMedicalHistory>

    @Transaction
    @Query("SELECT * FROM MedicalAppointment")
    suspend fun getMedicalAppointmentWithSymptom(): List<MedicalAppointmentWithSymptoms>

}