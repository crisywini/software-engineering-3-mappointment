package co.uniquindio.software3.mappointment.framework.database.doctor

import co.uniquindio.software3.data.doctor.PersistenceDoctorDataSource
import co.uniquindio.software3.domain.model.Doctor
import co.uniquindio.software3.mappointment.MedicalAppointmentApp

class PersistenceDoctorSource(private val context: MedicalAppointmentApp) :
    PersistenceDoctorDataSource {
    override suspend fun save(doctor: Doctor) {
        context.room.doctorDao().saveDoctor(doctor)
    }

    override suspend fun getById(id: String): Doctor? {
        return context.room.doctorDao().getDoctorById(id)
    }
}