package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import co.uniquindio.software3.domain.model.MedicalHistoryWithMedicalAppointment
import co.uniquindio.software3.domain.model.MedicalHistory

@Dao
interface MedicalHistoryDAO {

    @Query("SELECT * FROM MedicalHistory")
    suspend fun getAllMedicalHistories(): List<MedicalHistory>

    @Transaction
    @Query("SELECT * FROM MedicalHistory")
    suspend fun getMedicalHistoryWithMedicalAppointments(): List<MedicalHistoryWithMedicalAppointment>

    @Insert
    suspend fun saveMedicalHistory(medicalHistory: MedicalHistory)
}