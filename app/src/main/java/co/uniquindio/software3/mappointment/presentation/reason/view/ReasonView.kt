package co.uniquindio.software3.mappointment.presentation.reason.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepositoryImpl
import co.uniquindio.software3.data.reason.ReasonRepositoryImpl
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.Reason
import co.uniquindio.software3.mappointment.MedicalAppointmentApp
import co.uniquindio.software3.mappointment.R
import co.uniquindio.software3.mappointment.framework.database.medical_appointment.PersistenceMedicalAppointmentSource
import co.uniquindio.software3.mappointment.framework.database.reason.PersistenceReasonSource
import co.uniquindio.software3.mappointment.presentation.in_medical_appointment.view.InMedicalAppointmentView
import co.uniquindio.software3.mappointment.presentation.reason.presenter.ReasonPresenter
import co.uniquindio.software3.mappointment.presentation.reason.view.adapter.ReasonsAdapter
import co.uniquindio.software3.usecase.medicalappointment.GetMedicalAppointment
import co.uniquindio.software3.usecase.medicalappointment.UpdateMedicalAppointment
import co.uniquindio.software3.usecase.reason.GetAllReasons
import kotlinx.android.synthetic.main.activity_reason_view.*

class ReasonView : AppCompatActivity(), ReasonPresenter.View {

    private lateinit var app: MedicalAppointmentApp
    private lateinit var presenter: ReasonPresenter
    private var medicalAppointment: MedicalAppointment? = null
    private lateinit var medicalAppointmentId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reason_view)
        setTitle(R.string.reasons_view_title)
        app = applicationContext as MedicalAppointmentApp
        val persistenceReason = PersistenceReasonSource(app)
        val reasonRepository = ReasonRepositoryImpl(persistenceReason)
        val persistenceMedicalAppointment = PersistenceMedicalAppointmentSource(app)
        val medicalAppointmentRepository =
            MedicalAppointmentRepositoryImpl(persistenceMedicalAppointment)
        presenter = ReasonPresenter(
            this,
            GetAllReasons(reasonRepository),
            GetMedicalAppointment(medicalAppointmentRepository),
            UpdateMedicalAppointment(medicalAppointmentRepository)
        )
        getExtras()
        setUpRecyclerView()
    }

    private fun getExtras() {
        val bundle = intent.extras
        if (bundle != null) {
            medicalAppointmentId = bundle.get("medicalAppointmentId") as String
            presenter.setMedicalAppointment(medicalAppointmentId.toInt())
        }
    }

    override fun initialize(medicalAppointment: MedicalAppointment?) {
        this.medicalAppointment = medicalAppointment
    }

    override fun startMedicalHistoryActivity(view: View) {
        val reasonDescriptionTextView = view.findViewById<TextView>(R.id.reasonDescriptionTextView)
        val tag = reasonDescriptionTextView.tag.toString().toInt()
        medicalAppointment?.personalReasonId = tag
        presenter.updateMedicalAppointment(medicalAppointment!!)

        val intent = Intent(this, InMedicalAppointmentView::class.java)

        startActivity(intent)
    }

    private fun setUpRecyclerView() {
        val adapter = ReasonsAdapter(View.OnClickListener {
            startMedicalHistoryActivity(it)
        })
        reasonsRecyclerView.adapter = adapter
        reasonsRecyclerView.layoutManager = LinearLayoutManager(this)
        loadReasons()
    }

    private fun loadReasons() {
        presenter.getReasons()
    }

    override fun loadReasons(reasons: List<Reason>) {
        val adapter = reasonsRecyclerView.adapter as ReasonsAdapter
        adapter.setDataset(reasons)
    }


}