package co.uniquindio.software3.mappointment.presentation.in_medical_appointment.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.uniquindio.software3.mappointment.presentation.medical_history.view.MedicalHistoryFragment

class ViewPagerAdapter(fragmentManager: FragmentManager, behaviour: Int) :
    FragmentPagerAdapter(fragmentManager, behaviour) {
    private val FRAGMENTS: Array<Fragment> = arrayOf(MedicalHistoryFragment())
    override fun getCount(): Int {
        return FRAGMENTS.size
    }

    override fun getItem(position: Int): Fragment {
        return FRAGMENTS[position]
    }
}