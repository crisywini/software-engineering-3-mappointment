package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import co.uniquindio.software3.domain.model.Date
import co.uniquindio.software3.domain.model.DateAndMedicalAppointment

@Dao
interface DateDAO {
    @Insert
    suspend fun saveDate(date: Date)

    @Transaction
    @Query("SELECT * FROM Date")
    suspend fun getMedicalAppointmentsAndDates(): List<DateAndMedicalAppointment>

    @Transaction
    @Query("SELECT * FROM Date")
    suspend fun getMedicalAppointmentsAndDatesByUserId(): List<DateAndMedicalAppointment>

    @Query("SELECT * FROM Date WHERE date_id = :dateId LIMIT 1")
    suspend fun getDate(dateId: Int): Date?
}