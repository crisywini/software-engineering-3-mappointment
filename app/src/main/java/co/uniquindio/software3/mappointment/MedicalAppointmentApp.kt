package co.uniquindio.software3.mappointment

import android.app.Application
import androidx.room.Room
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import java.io.IOException

class MedicalAppointmentApp : Application() {
    lateinit var room: AppDatabase
        private set

   override fun onCreate() {
        super.onCreate()
        room = Room.databaseBuilder(this, AppDatabase::class.java, "DataBase").build()

    }

    override fun onTerminate() {
        super.onTerminate()
        try {
            room.close()
        }catch (e:IOException){

        }
    }
}