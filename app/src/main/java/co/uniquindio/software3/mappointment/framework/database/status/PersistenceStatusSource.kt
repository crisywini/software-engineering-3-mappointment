package co.uniquindio.software3.mappointment.framework.database.status

import co.uniquindio.software3.data.status.PersistenceStatusDataSource
import co.uniquindio.software3.domain.model.Status
import co.uniquindio.software3.mappointment.MedicalAppointmentApp

class PersistenceStatusSource(private val context: MedicalAppointmentApp) :
    PersistenceStatusDataSource {
    override suspend fun save(status: Status) {
        context.room.statusDao().saveStatus(status)
    }

    override suspend fun get(id: Int): Status? {
        return context.room.statusDao().getStatusById(id)
    }
}