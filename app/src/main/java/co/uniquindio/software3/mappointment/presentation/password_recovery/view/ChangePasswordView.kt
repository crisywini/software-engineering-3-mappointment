package co.uniquindio.software3.mappointment.presentation.password_recovery.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import co.uniquindio.software3.mappointment.R

class ChangePasswordView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
    }

    fun onClickChangePassword(view: View) {}
}