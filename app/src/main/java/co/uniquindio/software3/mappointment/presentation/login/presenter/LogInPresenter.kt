package co.uniquindio.software3.mappointment.presentation.login.presenter

import android.util.Log
import co.uniquindio.software3.domain.model.User
import co.uniquindio.software3.usecase.date.SaveDate
import co.uniquindio.software3.usecase.doctor.SaveDoctor
import co.uniquindio.software3.usecase.medicalappointment.SaveMedicalAppointment
import co.uniquindio.software3.usecase.reason.SaveReason
import co.uniquindio.software3.usecase.status.SaveStatus
import co.uniquindio.software3.usecase.user.LogInUser
import co.uniquindio.software3.usecase.user.SaveUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class LogInPresenter(
    private val view: View?,
    private var logInUser: LogInUser,
    private var saveUser: SaveUser,
    private var saveDate: SaveDate,
    private var saveStatus: SaveStatus,
    private var saveReason: SaveReason,
    private var saveDoctor: SaveDoctor,
    private var saveMedicalAppointment: SaveMedicalAppointment
) :
    CoroutineScope {
    interface View {
        fun showMedicalAppointmentsView(user: User)
        fun showError(errorMessage: String)
        fun cleanFields()
    }

    private val job = Job()//If one coroutine fail, all the coroutines fails

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job


    fun detachJob() {
        job.complete()
    }


    fun signInWithEmailAndPassword(email: String, password: String) {
        var user: User? = null
        launch {
            user = logInUser.invoke(email, password)
            if (user != null) {
                view?.cleanFields()
                view?.showMedicalAppointmentsView(user!!)
            } else {
                view?.showError("No se ha encontrado el usuario con email: $email")
            }
        }

    }

    fun insertUser() {
        launch {
            for (num in 1..5) {
                saveUser.invoke(
                    "$num", "prueba",
                    "prueba", "prueba@prueba.com", "prueba"
                )
            }
            Log.i("insertUser: ", "Usuarios insertados")
        }
    }

    fun insertStatus() {
        launch {
            saveStatus.invoke(1, "ENTRAR")
            saveStatus.invoke(2, "MODIFICAR")
        }
    }

    fun insertDates() {
        launch {
            for (num in 1..5) {
                saveDate.invoke(num, 21, 7, 2021, 10, 0, 8)
            }
        }
    }

    fun insertReasons() {
        launch {
            saveReason.invoke(1, "Chequeo")
            saveReason.invoke(2, "Revisi�n pre-especialista")
            saveReason.invoke(3, "Revisi�n por sintomas")
            saveReason.invoke(4, "Revisi�n por resultados")
        }
    }

    fun insertDoctors() {
        launch {
            saveDoctor.invoke(
                "123456", "Alejandro", "Restrepo"
            )
        }
    }

    fun insertMedicalAppointments() {
        launch {
            saveMedicalAppointment.invoke(
                1, 1, 1,
                1, "1", "123456"
            )
            saveMedicalAppointment.invoke(
                2, 1, 1,
                1, "1", "123456"
            )
            saveMedicalAppointment.invoke(
                3, 1, 1,
                2, "1", "123456"
            )
        }
    }

    fun insertMedicalHistorieS(){
        launch {

        }
    }

}