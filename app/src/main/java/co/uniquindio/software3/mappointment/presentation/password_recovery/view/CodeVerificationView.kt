package co.uniquindio.software3.mappointment.presentation.password_recovery.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import co.uniquindio.software3.mappointment.R

class CodeVerificationView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_verification)
    }

    fun onClickSendVerification(view: View) {}
}