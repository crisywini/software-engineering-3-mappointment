package co.uniquindio.software3.mappointment.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import co.uniquindio.software3.domain.model.DateAndMedicalAppointment
import co.uniquindio.software3.domain.model.Reason
import co.uniquindio.software3.domain.model.ReasonAndMedicalAppointment

@Dao
interface ReasonDAO {
    @Insert
    suspend fun saveReason(reason: Reason)

    @Query("SELECT * FROM Reason WHERE reason_id = :reasonId")
    suspend fun getReasonById(reasonId: Int): Reason?

    @Transaction
    @Query("SELECT * FROM Reason")
    suspend fun getMedicalAppointmentsAndReasons(): List<ReasonAndMedicalAppointment>

    @Query("SELECT * FROM Reason")
    suspend fun getReasons(): List<Reason>
}