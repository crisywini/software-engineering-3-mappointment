package co.uniquindio.software3.mappointment.framework.interactor.loginInteractor

import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

interface SignInInteractor {

    suspend fun signInWithEmailAndPassword(email: String, password: String)

}