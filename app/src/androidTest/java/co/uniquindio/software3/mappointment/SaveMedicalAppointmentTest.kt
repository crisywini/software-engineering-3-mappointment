package co.uniquindio.software3.mappointment

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import co.uniquindio.software3.mappointment.framework.database.dao.MedicalAppointmentDAO
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)

class SaveMedicalAppointmentTest {
    private lateinit var db: AppDatabase
    private lateinit var medicalAppointmentDao: MedicalAppointmentDAO

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        medicalAppointmentDao = db.medicalAppointmentDao()
    }

    @Before
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun saveMedicalAppointmentTest() = runBlocking {
        val medicalAppointment = MedicalAppointment(
            1, 1,
            1, 1, "1", "1"
        )
        medicalAppointmentDao.saveMedicalAppointment(medicalAppointment)
        val other = medicalAppointmentDao.getMedicalAppointmentById(1)
        Assert.assertNotNull(other)
    }


}