package co.uniquindio.software3.mappointment

import android.content.Context
import androidx.room.Dao
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.uniquindio.software3.domain.model.User
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import co.uniquindio.software3.mappointment.framework.database.dao.UserDAO
import co.uniquindio.software3.mappointment.framework.database.user.PersistenceUserSource
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class LoginTest {

    private lateinit var userDao: UserDAO
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        userDao = db.userDao()
    }

    @Before
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun logInTest() = runBlocking {
        /*val user = persistence.logInUser("prueba", "prueba")*/
        //persistence.save(User("prueba","prueba","prueba","prueba@gmail.com","prueba"))
        val user = User(
            "12345", "Kyle", "Toronto",
            "kyle@toronto.com", "kyletoronto1234"
        )
        userDao.insert(user)
        val otherUser =userDao.logIn("kyle@toronto.com", "kyletoronto1234")
        Assert.assertNotNull(otherUser)
    }


}