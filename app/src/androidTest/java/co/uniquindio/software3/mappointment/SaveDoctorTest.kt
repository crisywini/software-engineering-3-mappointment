package co.uniquindio.software3.mappointment

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.uniquindio.software3.domain.model.Doctor
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import co.uniquindio.software3.mappointment.framework.database.dao.DoctorDAO
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)

class SaveDoctorTest {
    private lateinit var db: AppDatabase
    private lateinit var doctorDao: DoctorDAO

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        doctorDao = db.doctorDao()
    }

    @Before
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun saveDoctorTest() = runBlocking {
        val doctor = Doctor("1094", "Alejandro", "Restrepo")
        doctorDao.saveDoctor(doctor)
        val other = doctorDao.getDoctorById("1094")

        Assert.assertNotNull(other)
    }
}