package co.uniquindio.software3.mappointment

import androidx.test.ext.junit.runners.AndroidJUnit4
import co.uniquindio.software3.domain.exception.FieldMalformedException
import co.uniquindio.software3.domain.exception.RequiredArgumentException
import co.uniquindio.software3.domain.model.User
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ArgumentValidatorTest {

    private lateinit var user:User

    @Test
    fun argumentRequiredTest(){
        try {
            user = User("", "Alejandro", "Loaiza", "email@hotmail.com", "prueba123")
            Assert.fail("Should fail because the id")
        } catch (e: RequiredArgumentException){
            Assert.assertTrue(e.message, true)
        }
    }

    @Test
    fun emailValidatorTest(){
        try {
            user = User("123456", "Alejandro", "Loaiza", "hotmail.com", "prueba123")
            Assert.fail("Should fail because the email")
        } catch (e: FieldMalformedException){
            Assert.assertTrue(e.message, true)
        }
    }

}