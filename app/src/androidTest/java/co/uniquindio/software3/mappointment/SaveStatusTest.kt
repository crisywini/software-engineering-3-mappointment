package co.uniquindio.software3.mappointment

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.uniquindio.software3.domain.model.Status
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import co.uniquindio.software3.mappointment.framework.database.dao.StatusDAO
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)

class SaveStatusTest {
    private lateinit var db: AppDatabase
    private lateinit var statusDao: StatusDAO

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        statusDao = db.statusDao()
    }

    @Before
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun saveStatusTest() = runBlocking {
        val status = Status(1, "ACCEPTED")
        statusDao.saveStatus(status)
        val other = statusDao.getStatusById(1)
        Assert.assertNotNull(other)
    }
}