package co.uniquindio.software3.mappointment

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.uniquindio.software3.domain.model.Reason
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import co.uniquindio.software3.mappointment.framework.database.dao.ReasonDAO
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)

class SaveReasonTest {
    private lateinit var db: AppDatabase
    private lateinit var reasonDao: ReasonDAO

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        reasonDao = db.reasonDao()
    }

    @Before
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun saveReasonTest() = runBlocking {
        val reason = Reason(1, "Medicina General")
        reasonDao.saveReason(reason)
        val other = reasonDao.getReasonById(1)
        Assert.assertNotNull(other)
    }

}