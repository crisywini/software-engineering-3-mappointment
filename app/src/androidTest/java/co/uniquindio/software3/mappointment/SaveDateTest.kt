package co.uniquindio.software3.mappointment

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import co.uniquindio.software3.domain.model.Date
import co.uniquindio.software3.mappointment.framework.database.AppDatabase
import co.uniquindio.software3.mappointment.framework.database.dao.DateDAO
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class SaveDateTest {
    private lateinit var db: AppDatabase
    private lateinit var dateDao: DateDAO

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        dateDao = db.dateDao()
    }

    @Before
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun saveDateTest() = runBlocking {
        val date = Date(1, 10, 5, 2000, 5, 1, 1)
        dateDao.saveDate(date)
        val other = dateDao.getDate(1)
        Assert.assertNotNull(other)
    }

}