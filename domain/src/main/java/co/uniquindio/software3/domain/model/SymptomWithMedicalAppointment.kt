package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class SymptomWithMedicalAppointment(
    @Embedded
    val symptom: Symptom,
    @Relation(
        parentColumn = "symptom_id",
        entityColumn = "medical_appointment_id",
        associateBy = Junction(MedicalAppointmentSymptomCrossRef::class)
    )
    val symptoms: List<Symptom>
)
