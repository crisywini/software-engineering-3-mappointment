package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Status(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "status_id")
    val statusId: Int,
    @ColumnInfo(name = "status_description")
    val description: String
)