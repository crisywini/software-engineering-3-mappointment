package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Relation


data class MedicalAppointmentWithMeasures(
    @Embedded
    val medicalAppointment: MedicalAppointment,
    @Relation(
        parentColumn = "medical_appointment_id",
        entityColumn = "medicalAppointmentOwnerId"
    )
    val measures: List<Measure>
)