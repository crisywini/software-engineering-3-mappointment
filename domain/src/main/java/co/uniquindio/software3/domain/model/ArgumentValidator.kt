package co.uniquindio.software3.domain.model

import co.uniquindio.software3.domain.exception.FieldMalformedException
import co.uniquindio.software3.domain.exception.RequiredArgumentException
import java.util.regex.Pattern

class ArgumentValidator {
    companion object{
        fun validateRequired(value: Any?, message: String) {
            if (value == null || value.toString().isEmpty()) {
                throw RequiredArgumentException(message)
            }
        }
        fun validateEmail(value: String, message: String){
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}\$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(value)
            if(!matcher.matches()){
                throw FieldMalformedException(message)
            }

        }
    }
}