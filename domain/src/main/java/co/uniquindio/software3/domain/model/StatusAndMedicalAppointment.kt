package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Relation

data class StatusAndMedicalAppointment(
    @Embedded
    val status: Status,
    @Relation(
        parentColumn = "status_id",
        entityColumn = "currentStatusId"
    ) val medicalAppointment: MedicalAppointment
)
