package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Measure(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "measure_id")
    val measureId: Int,
    val description: String,
    val value: Double,
    val medicalAppointmentOwnerId: Int
)