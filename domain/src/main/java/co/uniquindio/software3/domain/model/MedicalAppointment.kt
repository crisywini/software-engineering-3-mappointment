package co.uniquindio.software3.domain.model

import androidx.room.*

@Entity
data class MedicalAppointment(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "medical_appointment_id")
    val medicalAppointmentId: Int,
    var currentDateId: Int,
    var personalReasonId: Int,
    var currentStatusId: Int,
    var userOwnerId: String,
    var doctorOwnerId: String

) {
    override fun toString(): String {
        return "MedicalAppointment(medicalAppointmentId=$medicalAppointmentId, currentDateId=$currentDateId, personalReasonId=$personalReasonId, currentStatusId=$currentStatusId, userOwnerId='$userOwnerId', doctorOwnerId='$doctorOwnerId')"
    }
}