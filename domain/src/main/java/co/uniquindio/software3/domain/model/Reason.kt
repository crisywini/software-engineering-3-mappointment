package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Reason(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "reason_id")
    val reasonId: Int,
    @ColumnInfo(name = "reason_description")
    val reasonDescription: String
)