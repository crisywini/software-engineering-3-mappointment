package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MedicalAppointmentWithSymptoms(
    @Embedded
    val medicalAppointment: MedicalAppointment,
    @Relation(
        parentColumn = "medical_appointment_id",
        entityColumn = "symptom_id",
        associateBy = Junction(MedicalAppointmentSymptomCrossRef::class)
    )
    val symptoms: List<Symptom>
)