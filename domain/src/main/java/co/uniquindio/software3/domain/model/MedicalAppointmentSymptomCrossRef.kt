package co.uniquindio.software3.domain.model

import androidx.room.Entity

@Entity(primaryKeys = ["medical_appointment_id", "symptom_id"])
data class MedicalAppointmentSymptomCrossRef(
    val medicalAppointmentId: Int,
    val symptomId: Int
)
