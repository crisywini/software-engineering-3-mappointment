package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Relation

data class UserWithMedicalAppointments(
    @Embedded val user: User,
    @Relation(
        parentColumn = "user_id",
        entityColumn = "userOwnerId"
    )
    val medicalAppointments: List<MedicalAppointment>
) {
}