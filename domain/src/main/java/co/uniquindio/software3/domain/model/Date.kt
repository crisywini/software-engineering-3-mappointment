package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Date(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "date_id")
    val dateId: Int,
    val day: Int, val month: Int, val year: Int, val hour: Int, val minute: Int, val second: Int
){
    override fun toString(): String {
        return "$day/$month/$year $hour:$minute:$second"
    }
}
