package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(@PrimaryKey
                @ColumnInfo(name = "user_id")
                val userId:String,
                val name: String,
                @ColumnInfo(name = "last_name")
                val lastName: String,
                val email: String,
                val password:String) {
    companion object{
        const val REQUIRED_ID_MESSAGE = "El id es requerido"
        const val EMAIL_MALFORMED_MESSAGE = "Debes ingresar un correo electr�nico v�lido"
    }
    init{
        ArgumentValidator.validateRequired(userId, REQUIRED_ID_MESSAGE)
        ArgumentValidator.validateEmail(email, EMAIL_MALFORMED_MESSAGE)
    }
}