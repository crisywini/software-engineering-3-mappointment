package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Symptom(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "symptom_id")
    val symptomId: Int,
    val description: String
)