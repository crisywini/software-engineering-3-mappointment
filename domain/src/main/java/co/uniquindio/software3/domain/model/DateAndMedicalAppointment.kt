package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Relation

data class DateAndMedicalAppointment(
    @Embedded
    val date: Date,
    @Relation(
        parentColumn = "date_id",
        entityColumn = "currentDateId"
    )
    val medicalAppointment: MedicalAppointment
)