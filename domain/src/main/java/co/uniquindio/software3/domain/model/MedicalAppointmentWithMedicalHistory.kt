package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MedicalAppointmentWithMedicalHistory(
    @Embedded
    val medicalAppointment: MedicalAppointment,
    @Relation(
        parentColumn = "medical_appointment_id",
        entityColumn = "medical_history_id",
        associateBy = Junction(MedicalAppointmentMedicalHistoryCrossRef::class)
    )
    val medicalHistories: List<MedicalHistory>
)
