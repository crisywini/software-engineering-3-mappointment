package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Doctor(
    @PrimaryKey
    @ColumnInfo(name = "doctor_id")
    val doctorId: String,
    val name: String,
    val lastName: String
)
