package co.uniquindio.software3.domain.exception

class RequiredArgumentException(message: String?) : Exception(message) {

}