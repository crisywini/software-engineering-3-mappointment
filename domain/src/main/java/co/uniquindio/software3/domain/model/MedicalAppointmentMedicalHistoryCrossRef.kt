package co.uniquindio.software3.domain.model

import androidx.room.Entity

@Entity(primaryKeys = ["medical_appointment_id", "medical_history_id"])
data class MedicalAppointmentMedicalHistoryCrossRef(
    val medicalAppointmentId: Int,
    val medicalAppointmentHistoryId: Int
)