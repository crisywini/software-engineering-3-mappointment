package co.uniquindio.software3.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MedicalHistory(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "medical_history_id")
    val medicalHistoryId: Int,
    val description: String
)