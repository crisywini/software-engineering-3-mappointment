package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Relation

data class ReasonAndMedicalAppointment(
    @Embedded
    val reason: Reason,
    @Relation(
        parentColumn = "reason_id",
        entityColumn = "personalReasonId"
    )
    val medicalAppointment: MedicalAppointment
)
