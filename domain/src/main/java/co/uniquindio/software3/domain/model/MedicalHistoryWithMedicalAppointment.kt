package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MedicalHistoryWithMedicalAppointment(
    @Embedded
    val medicalHistory: MedicalHistory,
    @Relation(
        parentColumn = "medical_history_id",
        entityColumn = "medical_appointment_id",
        associateBy = Junction(MedicalAppointmentMedicalHistoryCrossRef::class)
    )
    val medicalAppointments: List<MedicalAppointment>
)