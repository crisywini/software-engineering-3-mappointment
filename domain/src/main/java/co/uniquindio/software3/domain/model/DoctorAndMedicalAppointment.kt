package co.uniquindio.software3.domain.model

import androidx.room.Embedded
import androidx.room.Relation

data class DoctorAndMedicalAppointment(
    @Embedded
    val doctor: Doctor,
    @Relation(
        parentColumn = "doctor_id",
        entityColumn = "doctorOwnerId"
    )
    val medicalAppointment: MedicalAppointment
)
