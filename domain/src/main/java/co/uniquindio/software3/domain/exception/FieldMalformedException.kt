package co.uniquindio.software3.domain.exception

class FieldMalformedException(errorMessage: String?) : Exception(errorMessage) {
}