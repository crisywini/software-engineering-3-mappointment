package co.uniquindio.software3.usecase.doctor

import co.uniquindio.software3.data.doctor.DoctorRepository

class SaveDoctor(private val doctorRepository: DoctorRepository) {
    suspend operator fun invoke(
        doctorId: String,
        name: String,
        lastName: String
    ) {
        doctorRepository.saveDoctor(doctorId, name, lastName)
    }
}