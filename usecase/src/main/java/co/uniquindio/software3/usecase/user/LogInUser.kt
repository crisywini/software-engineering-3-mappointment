package co.uniquindio.software3.usecase.user

import co.uniquindio.software3.data.user.UserRepository
import co.uniquindio.software3.domain.model.User

class LogInUser(private val userRepository: UserRepository) {
    suspend operator fun invoke(email: String, password: String): User? =
        userRepository.logInUser(email, password)
}