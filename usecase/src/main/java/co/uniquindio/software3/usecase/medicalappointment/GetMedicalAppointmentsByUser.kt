package co.uniquindio.software3.usecase.medicalappointment

import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepository
import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.UserWithMedicalAppointments

class GetMedicalAppointmentsByUser(private val medicalAppointmentRepository: MedicalAppointmentRepository) {

    suspend operator fun invoke(userId: String): List<MedicalAppointment> =
        medicalAppointmentRepository.getMedicalAppointmentsByUser(userId)
}