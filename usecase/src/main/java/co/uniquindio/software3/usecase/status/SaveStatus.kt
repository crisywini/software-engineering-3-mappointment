package co.uniquindio.software3.usecase.status

import co.uniquindio.software3.data.status.StatusRepository

class SaveStatus(private val statusRepository: StatusRepository) {
    suspend operator fun invoke(
        statusId: Int,
        description: String) {
        statusRepository.saveStatus(statusId, description)
    }
}