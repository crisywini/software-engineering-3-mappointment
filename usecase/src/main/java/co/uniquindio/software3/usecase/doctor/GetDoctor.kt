package co.uniquindio.software3.usecase.doctor

import co.uniquindio.software3.data.doctor.DoctorRepository

class GetDoctor(private val doctorRepository: DoctorRepository) {
    suspend operator fun invoke(id: String) = doctorRepository.getById(id)
}