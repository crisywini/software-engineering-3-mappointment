package co.uniquindio.software3.usecase.symptom

import co.uniquindio.software3.data.symptom.SymptomRepository

class SaveSymptom(private val symptomRepository: SymptomRepository) {
    suspend operator fun invoke(symptomId: Int, description: String) {
        symptomRepository.saveSymptom(symptomId, description)
    }
}