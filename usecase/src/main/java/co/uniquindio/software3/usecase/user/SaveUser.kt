package co.uniquindio.software3.usecase.user

import co.uniquindio.software3.data.user.UserRepository

class SaveUser(private val userRepository: UserRepository) {
    suspend operator fun invoke(id: String, name: String, lastName:String, email:String, password:String){
        userRepository.addUser(id, name, lastName, email, password)
    }
}