package co.uniquindio.software3.usecase.symptom

import co.uniquindio.software3.data.symptom.SymptomRepository

class GetAllSymptoms(private val symptomRepository: SymptomRepository) {
    suspend operator fun invoke() = symptomRepository.getAllSymptoms()
}