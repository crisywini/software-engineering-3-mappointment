package co.uniquindio.software3.usecase.medicalappointment

import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepository
import co.uniquindio.software3.domain.model.MedicalAppointment

class GetMedicalAppointment(private val medicalAppointmentRepository: MedicalAppointmentRepository) {
    suspend operator fun invoke(id: Int): MedicalAppointment? {
        return medicalAppointmentRepository.getMedicalAppointmentById(id)
    }
}