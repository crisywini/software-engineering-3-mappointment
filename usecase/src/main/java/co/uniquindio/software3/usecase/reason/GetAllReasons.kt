package co.uniquindio.software3.usecase.reason

import co.uniquindio.software3.data.reason.ReasonRepository

class GetAllReasons(private val reasonRepository: ReasonRepository) {
    suspend operator fun invoke() = reasonRepository.getReasons()
}