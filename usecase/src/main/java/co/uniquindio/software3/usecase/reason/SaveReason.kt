package co.uniquindio.software3.usecase.reason

import co.uniquindio.software3.data.reason.ReasonRepository

class SaveReason(private val reasonRepository: ReasonRepository) {
    suspend operator fun invoke(
        reasonId: Int,
        reasonDescription: String) {
        reasonRepository.saveReason(reasonId, reasonDescription)
    }
}