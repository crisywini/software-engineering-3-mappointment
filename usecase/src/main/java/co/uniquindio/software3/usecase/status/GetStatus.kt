package co.uniquindio.software3.usecase.status

import co.uniquindio.software3.data.status.StatusRepository

class GetStatus(private val statusRepository: StatusRepository) {
    suspend operator fun invoke(id: Int) = statusRepository.getStatusById(id)
}