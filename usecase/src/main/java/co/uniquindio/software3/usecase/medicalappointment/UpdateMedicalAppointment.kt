package co.uniquindio.software3.usecase.medicalappointment

import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepository
import co.uniquindio.software3.domain.model.MedicalAppointment

class UpdateMedicalAppointment(private val medicalAppointmentRepository: MedicalAppointmentRepository) {
    suspend operator fun invoke(medicalAppointment: MedicalAppointment) {
        medicalAppointmentRepository.updateMedicalAppointment(medicalAppointment)
    }
}