package co.uniquindio.software3.usecase.date

import co.uniquindio.software3.data.date.DateRepository

class GetDate(private val dateRepository: DateRepository) {
    suspend operator fun invoke(id: Int) = dateRepository.getDateById(id)
}