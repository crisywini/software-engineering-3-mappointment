package co.uniquindio.software3.usecase.medical_history

import co.uniquindio.software3.data.medical_history.MedicalHistoryRepository

class GetAllMedicalHistories(private val medicalHistoryRepository: MedicalHistoryRepository) {
    suspend operator fun invoke() = medicalHistoryRepository.getMedicalHistories()
}