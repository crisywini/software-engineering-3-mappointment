package co.uniquindio.software3.usecase.medicalappointment

import co.uniquindio.software3.data.medical_appointment.MedicalAppointmentRepository


class SaveMedicalAppointment(private val medicalAppointmentRepository: MedicalAppointmentRepository) {
    suspend operator fun invoke(
        medicalAppointmentId: Int,
        currentDateId: Int,
        personalReasonId: Int,
        currentStatusId: Int,
        userOwnerId: String,
        doctorOwnerId: String
    ) {
        medicalAppointmentRepository.saveMedicalAppointment(
            medicalAppointmentId,
            currentDateId,
            personalReasonId,
            currentStatusId,
            userOwnerId,
            doctorOwnerId
        )
    }
}