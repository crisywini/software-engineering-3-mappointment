package co.uniquindio.software3.usecase.date

import co.uniquindio.software3.data.date.DateRepository

class SaveDate(private val dateRepository: DateRepository) {
    suspend operator fun invoke(
        dateId: Int,
        day: Int, month: Int, year: Int, hour: Int, minute: Int, second: Int
    ) {
        dateRepository.saveDate(
            dateId,
            day,
            month,
            year,
            hour,
            minute,
            second
        )

    }
}