package co.uniquindio.software3.data.medical_appointment

import co.uniquindio.software3.domain.model.MedicalAppointment

class MedicalAppointmentRepositoryImpl(private val persistenceMedicalAppointmentDataSource: PersistenceMedicalAppointmentDataSource) :
    MedicalAppointmentRepository {

    override suspend fun getMedicalAppointmentsByUser(userId: String): List<MedicalAppointment> {
        return persistenceMedicalAppointmentDataSource.getMedicalAppointmentsByUser(userId)
    }

    override suspend fun saveMedicalAppointment(
        medicalAppointmentId: Int,
        currentDateId: Int,
        personalReasonId: Int,
        currentStatusId: Int,
        userOwnerId: String,
        doctorOwnerId: String
    ) {
        persistenceMedicalAppointmentDataSource.save(
            MedicalAppointment(
                medicalAppointmentId,
                currentDateId,
                personalReasonId,
                currentStatusId,
                userOwnerId,
                doctorOwnerId
            )
        )
    }

    override suspend fun getMedicalAppointmentById(medicalAppointmentId: Int): MedicalAppointment? {
        return persistenceMedicalAppointmentDataSource.getMedicalAppointmentById(
            medicalAppointmentId
        )
    }

    override suspend fun updateMedicalAppointment(medicalAppointment: MedicalAppointment) {
        persistenceMedicalAppointmentDataSource.updateMedicalAppointment(medicalAppointment)
    }
}