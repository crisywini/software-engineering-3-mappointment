package co.uniquindio.software3.data.user

import co.uniquindio.software3.domain.model.User

interface PersistenceUserDataSource {

    suspend fun logInUser(email:String, password:String): User?
    suspend fun save(user:User)
    suspend fun getById(id: String):User?
    suspend fun update(user: User)
    suspend fun remove(user: User):User?
    suspend fun getAll():List<User>

    suspend fun exists(id: String): Boolean
}