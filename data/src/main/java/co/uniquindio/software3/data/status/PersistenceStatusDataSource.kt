package co.uniquindio.software3.data.status

import co.uniquindio.software3.domain.model.Status

interface PersistenceStatusDataSource {
    suspend fun save(status: Status)
    suspend fun get(id: Int): Status?
}