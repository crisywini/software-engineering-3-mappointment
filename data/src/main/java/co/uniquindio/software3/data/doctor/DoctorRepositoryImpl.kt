package co.uniquindio.software3.data.doctor

import co.uniquindio.software3.domain.model.Doctor

class DoctorRepositoryImpl(private val persistenceDoctorDataSource: PersistenceDoctorDataSource) :
    DoctorRepository {
    override suspend fun saveDoctor(doctorId: String, name: String, lastName: String) {
        persistenceDoctorDataSource.save(Doctor(doctorId, name, lastName))
    }

    override suspend fun getById(doctorId: String): Doctor? {
        return persistenceDoctorDataSource.getById(doctorId)
    }
}