package co.uniquindio.software3.data.date

import co.uniquindio.software3.domain.model.Date

interface DateRepository {
    suspend fun saveDate(
        dateId: Int,
        day: Int, month: Int, year: Int, hour: Int, minute: Int, second: Int
    )

    suspend fun getDateById(dateId: Int): Date?
}