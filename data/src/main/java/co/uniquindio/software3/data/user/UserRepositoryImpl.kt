package co.uniquindio.software3.data.user

import co.uniquindio.software3.data.exception.EntityNullException
import co.uniquindio.software3.domain.model.User

class UserRepositoryImpl(private val persistenceUserDataSource: PersistenceUserDataSource):UserRepository {
    companion object{
        const val NON_EXISTENCE_MESSAGE = "Este usuario no existe!"
    }
    override suspend fun addUser(
        id: String,
        name: String,
        lastName: String,
        email: String,
        password: String
    ) {
        val user = User(id, name, lastName, email, password)
        persistenceUserDataSource.save(user)
    }

    override suspend fun deleteUser(user:User): User? {
        verifyUserExistence(user)
        return persistenceUserDataSource.remove(user)
    }

    override suspend fun getAllUsers(): List<User> {
        return persistenceUserDataSource.getAll()
    }

    override suspend fun getUserById(id: String): User? {
        return persistenceUserDataSource.getById(id)
    }

    override suspend fun logInUser(email: String, password: String): User? {
        return persistenceUserDataSource.logInUser(email, password)
    }
    private suspend fun verifyUserExistence(user: User){
        if(!persistenceUserDataSource.exists(user.userId)){
            throw EntityNullException(NON_EXISTENCE_MESSAGE)
        }
    }

}