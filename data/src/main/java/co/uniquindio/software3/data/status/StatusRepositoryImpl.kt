package co.uniquindio.software3.data.status

import co.uniquindio.software3.domain.model.Status

class StatusRepositoryImpl(private val persistenceStatusDataSource: PersistenceStatusDataSource) :
    StatusRepository {
    override suspend fun saveStatus(
        statusId: Int,
        description: String
    ) {
        persistenceStatusDataSource.save(Status(statusId, description))
    }

    override suspend fun getStatusById(id: Int): Status? {
        return persistenceStatusDataSource.get(id)
    }
}