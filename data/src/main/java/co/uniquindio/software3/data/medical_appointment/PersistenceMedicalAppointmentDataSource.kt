package co.uniquindio.software3.data.medical_appointment

import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.UserWithMedicalAppointments

interface PersistenceMedicalAppointmentDataSource {
    suspend fun getMedicalAppointmentsByUser(userId: String): List<MedicalAppointment>
    suspend fun save(medicalAppointment: MedicalAppointment)
    suspend fun getMedicalAppointmentById(medicalAppointmentId: Int): MedicalAppointment?
    suspend fun updateMedicalAppointment(medicalAppointment: MedicalAppointment)
}