package co.uniquindio.software3.data.doctor

import co.uniquindio.software3.domain.model.Doctor

interface PersistenceDoctorDataSource {

    suspend fun save(doctor: Doctor)
    
    suspend fun getById(id: String): Doctor?
}