package co.uniquindio.software3.data.status

import co.uniquindio.software3.domain.model.Status

interface StatusRepository {
    suspend fun saveStatus(
        statusId: Int,
        description: String)
    suspend fun getStatusById(id: Int):Status?
}