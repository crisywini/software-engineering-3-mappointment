package co.uniquindio.software3.data.reason

import co.uniquindio.software3.domain.model.Reason

class ReasonRepositoryImpl(private val persistenceReasonDataSource: PersistenceReasonDataSource) :
    ReasonRepository {
    override suspend fun saveReason(
        reasonId: Int,
        reasonDescription: String
    ) {
        persistenceReasonDataSource.save(
            Reason(
                reasonId,
                reasonDescription
            )
        )
    }

    override suspend fun getReasons(): List<Reason> {
        return persistenceReasonDataSource.getAll()
    }
}