package co.uniquindio.software3.data.reason

import co.uniquindio.software3.domain.model.Reason

interface ReasonRepository {
    suspend fun saveReason(
        reasonId: Int,
        reasonDescription: String
    )

    suspend fun getReasons():List<Reason>
}