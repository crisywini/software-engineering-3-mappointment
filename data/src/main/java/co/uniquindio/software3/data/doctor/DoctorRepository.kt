package co.uniquindio.software3.data.doctor

import co.uniquindio.software3.domain.model.Doctor

interface DoctorRepository {

    suspend fun saveDoctor(
        doctorId: String,
        name: String,
        lastName: String
    )
    suspend fun getById(doctorId:String):Doctor?
}