package co.uniquindio.software3.data.symptom

import co.uniquindio.software3.domain.model.Symptom

interface PersistenceSymptomDataSource {
    suspend fun saveSymptom(symptom: Symptom)
    suspend fun getAllSymptoms(): List<Symptom>
}