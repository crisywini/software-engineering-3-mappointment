package co.uniquindio.software3.data.user

import co.uniquindio.software3.domain.model.User

interface UserRepository {

    suspend fun addUser(id: String, name: String, lastName:String, email:String, password:String)
    suspend fun deleteUser(user: User): User?
    suspend fun getAllUsers(): List<User>
    suspend fun getUserById(id:String):User?
    suspend fun logInUser(email: String, password:String): User?
}