package co.uniquindio.software3.data.date

import co.uniquindio.software3.domain.model.Date

class DateRepositoryImpl(private val persistenceDateDataSource: PersistenceDateDataSource) :
    DateRepository {
    override suspend fun saveDate(
        dateId: Int,
        day: Int,
        month: Int,
        year: Int,
        hour: Int,
        minute: Int,
        second: Int
    ) {
        val date = Date(dateId, day, month, year, hour, minute, second)
        persistenceDateDataSource.save(date)
    }

    override suspend fun getDateById(dateId: Int): Date? {
        return persistenceDateDataSource.get(dateId)
    }

}