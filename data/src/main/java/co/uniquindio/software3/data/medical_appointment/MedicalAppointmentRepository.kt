package co.uniquindio.software3.data.medical_appointment

import co.uniquindio.software3.domain.model.MedicalAppointment
import co.uniquindio.software3.domain.model.UserWithMedicalAppointments

interface MedicalAppointmentRepository {

    suspend fun getMedicalAppointmentsByUser(userId: String): List<MedicalAppointment>
    suspend fun saveMedicalAppointment(
        medicalAppointmentId: Int,
        currentDateId: Int,
        personalReasonId: Int,
        currentStatusId: Int,
        userOwnerId: String,
        doctorOwnerId: String
    )

    suspend fun getMedicalAppointmentById(medicalAppointmentId: Int): MedicalAppointment?
    suspend fun updateMedicalAppointment(
        medicalAppointment: MedicalAppointment
    )

}