package co.uniquindio.software3.data.medical_history

import co.uniquindio.software3.domain.model.MedicalHistory

interface MedicalHistoryRepository {

    suspend fun getMedicalHistories(): List<MedicalHistory>
    suspend fun saveMedicalHistory(medicalHistoryId: Int, description: String)
}