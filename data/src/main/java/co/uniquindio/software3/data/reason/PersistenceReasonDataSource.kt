package co.uniquindio.software3.data.reason

import co.uniquindio.software3.domain.model.Reason

interface PersistenceReasonDataSource {
    suspend fun save(reason:Reason)
    suspend fun getAll(): List<Reason>
}