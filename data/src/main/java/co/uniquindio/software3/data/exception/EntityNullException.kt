package co.uniquindio.software3.data.exception

class EntityNullException(message: String?) : Exception(message) {
}