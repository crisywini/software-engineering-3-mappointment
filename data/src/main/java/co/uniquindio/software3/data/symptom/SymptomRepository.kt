package co.uniquindio.software3.data.symptom

import co.uniquindio.software3.domain.model.Symptom

interface SymptomRepository {
    suspend fun saveSymptom(symptomId: Int, description: String)
    suspend fun getAllSymptoms(): List<Symptom>
}