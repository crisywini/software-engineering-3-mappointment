package co.uniquindio.software3.data.date

import co.uniquindio.software3.domain.model.Date

interface PersistenceDateDataSource {
    suspend fun save(date: Date)
    
    suspend fun get(id: Int): Date?
}