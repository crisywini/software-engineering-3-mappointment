package co.uniquindio.software3.data.symptom

import co.uniquindio.software3.domain.model.Symptom

class SymptomRepositoryImpl(private val persistenceSymptomDataSource: PersistenceSymptomDataSource) :
    SymptomRepository {
    override suspend fun saveSymptom(symptomId: Int, description: String) {
        persistenceSymptomDataSource.saveSymptom(Symptom(symptomId, description))
    }

    override suspend fun getAllSymptoms(): List<Symptom> =
        persistenceSymptomDataSource.getAllSymptoms()
}