package co.uniquindio.software3.data.medical_history

import co.uniquindio.software3.domain.model.MedicalHistory

interface PersistenceMedicalHistoryDataSource {
    suspend fun getMedicalHistories(): List<MedicalHistory>
    suspend fun saveMedicalHistory(medicalHistory: MedicalHistory)
}