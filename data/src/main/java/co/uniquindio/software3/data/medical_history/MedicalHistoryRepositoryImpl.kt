package co.uniquindio.software3.data.medical_history

import co.uniquindio.software3.domain.model.MedicalHistory

class MedicalHistoryRepositoryImpl(private val persistenceMedicalHistoryDataSource: PersistenceMedicalHistoryDataSource) :
    MedicalHistoryRepository {
    override suspend fun getMedicalHistories(): List<MedicalHistory> {
        return persistenceMedicalHistoryDataSource.getMedicalHistories()
    }

    override suspend fun saveMedicalHistory(medicalHistoryId: Int, description: String) {
        persistenceMedicalHistoryDataSource.saveMedicalHistory(
            MedicalHistory(
                medicalHistoryId,
                description
            )
        )
    }
}